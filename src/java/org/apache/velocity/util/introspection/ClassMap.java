package org.apache.velocity.util.introspection;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.lang.reflect.Method;

/**
   Map that is capable of finding method based on its name and required parameters.
   The map always operates bound to specific class.
 */
public interface ClassMap
{
    /**
     * Returns method of with given name and that accepts provided parameters.
     * To obtain the method no sub/super classing is taken in to account; this means that arguments in params table have to
     * exactly match method that has to be invoked. Except:
     * <li>
     *     <ul><b>NULL</b> - is always assumed to be Object</ul>
     *     <ul><b>Boxed types</b> -
     *     <p>will match primitive and boxed argument.
     *     This means that to invoke method get(int) you must pass as params parameter new Object[]{Integer.valueOf(1)}
     *     </p></ul>
     * </li>
     * @param name name of the method
     * @param params array of parameters which classes should be used to localise method
     * @return located method instance
     * @throws MethodMap.AmbiguousException when more than one method meets the criteria
     */
    Method findMethod(String name, Object[] params) throws MethodMap.AmbiguousException;
}
